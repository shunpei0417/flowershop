class AdminController < ApplicationController
    def admin_top
    end
    def access
        @access = Access.new
    end
    def access_save
        #入力ページに入力した値を取得
        puts params[:zipcode]
        puts params[:address]
        puts params[:tel]
        puts params[:mail]
        #入力した情報をデーターベースに保存
        @access = Access.new(zipcode:params[:zipcode],address:params[:address],tel:params[:tel],mail:params[:mail])
        @access.save
        redirect_to admin_access_path
    end
    def add_work
        @work = Work.new
    end 
    def work_save
        @work = Work.new(work_params)
        @work.save
        redirect_to add_work_path
    end
    def course_management
        #既にあるデータをデータベースから持ってくる。
        @now = OneDay.find_by_id(1)
     #   @one_day = OneDay.new
    end
    def course_change
        # viewでname="number"と書いてある入力ボックスに入力された値を取得。
        @number = params[:number]
        # viewでname="price_one"と書いてある入力ボックスに入力された値を取得。
        @price_one = params[:price_one]
        # viewでname="price_pair"と書いてある入力ボックスに入力された値を取得。
        @price_pair = params[:price_pair]
        # viewでname="comment"と書いてある入力ボックスに入力された値を取得。
        @comment = params[:comment]
        #エラー処理
        #@numberが入力されていなければエラー
        if @number == ''
            flash[:error] = '人数が入力されていません'
            redirect_to course_management_path
        elsif @price_one == ''
            flash[:error] = 'ひとり価格が入力されていません'
            redirect_to course_management_path
        elsif @price_pair == ''
            flash[:error] = 'ペア価格が入力されていません'
            redirect_to course_management_path
        elsif @comment == ''
            flash[:error] = 'コメントが入力されていません'
            redirect_to course_management_path
        else
            # p 'ここを見ろ'
            # p @number
            #既にあるデータをデータベースから持ってくる。
            @one_day = OneDay.find_by_id(1)
            # p @one_day.number
            # p @one_day.comment
            #入力された値によって更新する
            @one_day.update(number:@number, price_one:@price_one, price_pair:@price_pair, comment:@comment)
            flash[:sucess] = 'コース情報を変更しました'
            redirect_to admin_top_path
        end
    end
    #bridalsの内容を更新する画面を表示する
    def bridal_management 
        #modelを使ってdbからデータを持ってくる。
        #bridalsテーブルからIDが１データの塊を持ってくる。それをhogeと名付ける。
        @hoge = Bridal.find_by_id(1)
        #hogeから日時情報を引き出して表示
        p 'ここを見ろ'
        p @hoge.id
        p @hoge.datetime
        p @hoge.entry_price
        p @hoge.four_times_price
        p @hoge.comment
    end
    #ブライダル・アートフラワーコースの情報を変更する
    def bridal_change
        p 'ここを通過'
        #入力された値を取得する。
        datetime = params[:datetime]
        p datetime
        entry_price = params[:entry_price]
        p entry_price
        four_times_price = params[:four_times_price]
        p four_times_price
        comment = params[:comment]
        p comment
        #入力された値を元にテーブルの情報を書き換える
        #まず、bridals tableからID１の情報の塊を抜き出す
        @hoge = Bridal.find_by_id(1)
        #@hogeの値を更新する
        @hoge.update(datetime:datetime,entry_price:entry_price,four_times_price:four_times_price,comment:comment)
        redirect_to admin_top_path
    end
    #資格取得講座管理
    def certifieds_management
        #modelを使ってdbからデータを持ってくる。
        @hogehoge = Certified.find_by_id(1)
        p @hogehoge.id
        p @hogehoge.datetime
        p @hogehoge.four_times_price
        p @hogehoge.comment
    end
    def certifieds_change
        p 'ここを通過'
        datetime = params[:datetime]
        p datetime
        four_times_price = params[:four_times_price]
        p four_times_price
        comment = params[:comment]
        p comment
        #db値を変更する
        #①今あるデータを持ってくる
        @hogehoge = Certified.find_by_id(1)
        #②入力された値(93,95,97の３つの値)で、@hogehogeのカラム情報を上書きする
        @hogehoge.update(datetime:datetime,four_times_price:four_times_price,comment:comment)
        #情報が無事に変更されたメッセージを保存する
        flash[:sucess] = '資格取得情報を変更しました'
        redirect_to admin_top_path
    end
    #ストロングパラメーター
    def work_params
      params.require(:work).permit(:name, :image)
    end
    def notice_params
         params.require(:notice).permit(:publication_date, :notice)
    end 
    #お知らせの登録
    def notice
        @notice=Notice.new
    end
    def notice_save
        @notice = Notice.new(notice_params)
        @notice.save
      redirect_to admin_top_path
    end
end
