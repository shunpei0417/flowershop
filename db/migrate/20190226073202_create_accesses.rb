class CreateAccesses < ActiveRecord::Migration[5.2]
  def change
    create_table :accesses do |t|
      t.string :zipcode
      t.string :address
      t.string :tel
      t.string :mail

      t.timestamps
    end
  end
end
