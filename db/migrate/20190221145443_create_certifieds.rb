class CreateCertifieds < ActiveRecord::Migration[5.2]
  def change
    create_table :certifieds do |t|
      t.string :datetime
      t.integer :four_times_price
      t.string :comment

      t.timestamps
    end
  end
end
