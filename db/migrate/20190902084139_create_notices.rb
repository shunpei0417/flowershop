class CreateNotices < ActiveRecord::Migration[5.2]
  def change
    create_table :notices do |t|
      t.date :publication_date
      t.string :notice

      t.timestamps
    end
  end
end
