class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :mail
      t.integer :course
      t.string :contact
      t.boolean :positions
      t.boolean :know
      t.string :contact
      t.text :comment

      t.timestamps
    end
  end
end
