class CreateOneDays < ActiveRecord::Migration[5.2]
  def change
    create_table :one_days do |t|
      t.integer :number
      t.integer :price_one
      t.integer :price_pair
      t.string :comment

      t.timestamps
    end
  end
end
