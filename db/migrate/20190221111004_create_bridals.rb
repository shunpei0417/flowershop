class CreateBridals < ActiveRecord::Migration[5.2]
  def change
    create_table :bridals do |t|
      t.string :datetime
      t.integer :entry_price
      t.integer :four_times_price
      t.string :comment

      t.timestamps
    end
  end
end
